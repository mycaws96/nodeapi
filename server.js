const express = require('express');
const app = express();
const sql = require('mssql');
const port = 5000;
const { MongoClient } = require('mongodb');
const eslint = require('eslint')

app.listen(port, (req, res) => {
    console.log(`App listening to ${port}`);
})

app.get('/app', (req, res) => {
    res.send('Welcome')
})

app.get('/all', (req, res) => {

    res.send('All result');

})

app.post('/post', (req, res) => {

    res.send('POST SUCCESS');

})

app.put('/put', (req, res) => {

    res.send('PUT Success');

})